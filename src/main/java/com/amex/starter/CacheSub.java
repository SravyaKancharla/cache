package com.amex.starter;

/***created by akshaytan2@gmail.com tanoaks ***/
import io.vertx.core.Future;
import redis.clients.jedis.JedisPubSub;
import java.util.HashMap;
import java.util.function.BiConsumer;


public class CacheSub extends JedisPubSub {

  HashMap<String,String> localMap= new HashMap<>();

  final Future<String> handler;
  final BiConsumer<String,String> consumer;

  CacheSub(Future<String> handler, BiConsumer<String,String> consumer){
    this.handler=handler;
    this.consumer=consumer;
  }


  @Override
  public void onMessage(String channel, String message) {


    if(message!=null&&!message.isEmpty())
    localMap.put(channel,message);



    //rest of excution



  }
  public void onSubscribe(String channel, int subscribedChannels) {
    if(subscribedChannels==1){

      //call service time
      try {
        // response
        handler.onComplete(result->{
          consumer.accept(channel,result.result());

        });


      } catch (Exception e) {
        e.printStackTrace();
      }






    }else{


      if(localMap.containsKey(channel)){


      }


    }

    System.out.println("sub");

  }


  public void onUnsubscribe(String channel, int subscribedChannels) {


    if(subscribedChannels==1)
    {

      localMap.remove(channel);
    }

    //
    System.out.println("unsub");



  }





}
