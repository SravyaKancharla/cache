package com.amex.starter;

/***created by akshaytan2@gmail.com tanoaks ***/
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.time.Instant;

public class HttpServerVerticle extends AbstractVerticle {

  EventBus eb;

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    eb = vertx.eventBus();
    Router router = Router.router(vertx);
    router.get("/").handler(this::start);
    router.get("/pub").handler(this::publish);
    Promise<String> listnerOne = Promise.promise();


    vertx.deployVerticle(new Listener(), new DeploymentOptions()
      .setInstances(1).setWorker(true).setWorkerPoolSize(7), listnerOne);




        vertx.deployVerticle(new ListenerPublisher(), new DeploymentOptions()
          .setInstances(1).setWorker(true).setWorkerPoolSize(7));





    vertx.createHttpServer().requestHandler(router)
      .listen(8888, http -> {
        if (http.succeeded()) {
          startPromise.complete();
          System.out.println("HTTP server started on port 8888");
        } else {
          startPromise.fail(http.cause());
        }
      });
  }

  private void publish(RoutingContext routingContext) {

    JsonObject data = new JsonObject();
    data.put("time", Instant.now().toEpochMilli());
    eb.publish("pub", data);
    routingContext.response().end("Hello from Vert.x!");
  }

  private void start(RoutingContext routingContext) {

    JsonObject data = new JsonObject();
    data.put("time", Instant.now().toEpochMilli());
    eb.publish("ping", data);
    routingContext.response().end("Hello from Vert.x!");
  }
}
