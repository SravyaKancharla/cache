package com.amex.starter;
/***created by akshaytan2@gmail.com tanoaks ***/

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;

public class Listener extends AbstractVerticle {

  EventBus eb;
  final JedisPoolConfig poolConfig = buildPoolConfig();
  JedisPool jedisPool = new JedisPool(poolConfig, "localhost");

  private JedisPoolConfig buildPoolConfig() {
    final JedisPoolConfig poolConfig = new JedisPoolConfig();
    poolConfig.setMaxTotal(128);
    poolConfig.setMaxIdle(128);
    poolConfig.setMinIdle(16);
    poolConfig.setTestOnBorrow(true);
    poolConfig.setTestOnReturn(true);
    poolConfig.setTestWhileIdle(true);
    poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
    poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
    poolConfig.setNumTestsPerEvictionRun(3);
    poolConfig.setBlockWhenExhausted(true);
    return poolConfig;
  }


  @Override
  public void start(Promise<Void> fut) {

    eb = vertx.eventBus();
    System.out.println("Listner started");
    vertx.executeBlocking(x -> {
      eb.consumer("ping", message -> {

        System.out.println("Received message In Listener: " + message.body());
        // Now send back reply

        vertx.<JedisPubSub>executeBlocking(messageRequest -> {

          Jedis jedis = jedisPool.getResource();


          System.out.println("test");
          CompletableFuture completableFuture = CompletableFuture.runAsync(() -> {
            Future future = Future.future(j->{

              // call service here

            });
            CacheSub jedisPubSub = new CacheSub(future,(a,y)->{
              jedisPool.getResource().publish(a,y);
            });
            vertx.setTimer(60000, timedResult -> {
              jedisPubSub.unsubscribe();
            });
            jedis.subscribe(jedisPubSub, message.body().toString());

          });


        }, y -> {


          message.reply("pong!" + message.body());
        });


      });
    }, y -> {
      System.out.println("completed");
    });


    System.out.println("Listener started to lisen");
  }
}
