package com.amex.starter;

/***created by akshaytan2@gmail.com tanoaks ***/

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

import java.time.Duration;

public class ListenerPublisher extends AbstractVerticle {

  EventBus eb;
  final JedisPoolConfig poolConfig = buildPoolConfig();
  JedisPool jedisPool = new JedisPool(poolConfig, "localhost");

  private JedisPoolConfig buildPoolConfig() {
    final JedisPoolConfig poolConfig = new JedisPoolConfig();
    poolConfig.setMaxTotal(128);
    poolConfig.setMaxIdle(128);
    poolConfig.setMinIdle(16);
    poolConfig.setTestOnBorrow(true);
    poolConfig.setTestOnReturn(true);
    poolConfig.setTestWhileIdle(true);
    poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
    poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
    poolConfig.setNumTestsPerEvictionRun(3);
    poolConfig.setBlockWhenExhausted(true);
    return poolConfig;
  }


  @Override
  public void start(Promise<Void> fut) {

    eb = vertx.eventBus();

    eb.consumer("pub", message -> {

      System.out.println("Received message In Listener: " + message.body());
      // Now send back reply
      Jedis jedis=jedisPool.getResource();
      jedis.publish("channel","i got a tea request");
      message.reply("pong!" + message.body());
    });

    System.out.println("Listener started to lisen");
  }
}
